let todoItems = [];

//function to prevent XSS injection
let sanitizeHTML = (str) => {
    var temp = document.createElement('div');
    temp.textContent = str;
    return temp.innerHTML;
}

//function for adding task in todolist
function addTodo(text) {
    const todo = {
        text,
        checked: false,
        id: Date.now,
    };

    todoItems.push(todo);
    
    //rendering list
    let todoText = sanitizeHTML(todo.text);
    const list = document.getElementById('js-todo-list');
    list.insertAdjacentHTML('beforeend', `
        <li class="todo-item" data-key="${todo.id}">
            <i class="material-icons">check_box_outline_blank</i>
            <span class="list-text">${todoText}</span>
            <i class="material-icons">clear</i>
        </li>
    `);
}

//input data
const form = document.querySelector('.js-form');
form.addEventListener('submit', event => {
    event.preventDefault();
    const input = document.querySelector('.js-todo-input');
    const text = input.value.trim();
    if(text !== ''){
        addTodo(text);
        input.value = '';
        input.focus();
        input.setAttribute('placeholder', 'Enter a new todo item');
        input.setAttribute('style', 'color: #424242')
    } else {
        input.setAttribute('placeholder', 'please fill this required field');
        input.setAttribute('style', 'color: red')
    }
})

const task = document.getElementsByClassName(todo-item);
task.addEventListener("click", )